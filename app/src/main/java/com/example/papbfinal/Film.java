package com.example.papbfinal;

public class Film {
    String id, judul, desc;

    public Film() {
    }

    public Film(String judul, String desc) {
        this.judul = judul;
        this.desc = desc;
    }

    public Film(String id, String judul, String desc) {
        this.id = id;
        this.judul = judul;
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
