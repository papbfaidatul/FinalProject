package com.example.papbfinal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    Context con;
    List<Film> film;
    MainActivity main;
    FirebaseFirestore ff = FirebaseFirestore.getInstance();
    ProgressDialog dial;

    public Adapter(Context con, List<Film> film) {
        this.con = con;
        this.film = film;

    }

    @NonNull
    @Override
    public Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(con).inflate(R.layout.row, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.ViewHolder holder, int position) {
        Film f = film.get(position);
        holder.jud.setText(f.getJudul());

        dial = new ProgressDialog(con);
        dial.setCancelable(false);
        dial.setTitle("load");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getRootView().getContext());
                View popup = LayoutInflater.from(v.getRootView().getContext()).inflate(R.layout.customdialog, null);
                TextView DetJudul = popup.findViewById(R.id.Judul);
                TextView DetDesc = popup.findViewById(R.id.Desc);
                DetJudul.setText(f.getJudul());
                DetDesc.setText(f.getDesc());
                builder.setView(popup);
                builder.setCancelable(true);
                builder.show();

                Button update = popup.findViewById(R.id.btUpdate);
                update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dial.setMessage("Updating Data..");
                        dial.show();
                        String judul = DetJudul.getText().toString().trim();
                        String desc = DetDesc.getText().toString().trim();
                        String id = f.getId();
                        ff.collection("Film").document(id)
                                .update("judul", judul,"desc", desc)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        Toast.makeText(con, "Movie was Succesfully Updated.", Toast.LENGTH_SHORT).show();
                                        dial.dismiss();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(con, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                        dial.dismiss();
                                    }
                                });
                    }
                });

                Button delete = popup.findViewById(R.id.btDelete);
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dial.setMessage("Deleting Data..");
                        dial.show();
                        String id = f.getId();
                        ff.collection("Film").document(id)
                                .delete()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        Toast.makeText(con, "Movie was Succesfully Deleted.", Toast.LENGTH_SHORT).show();
                                        dial.dismiss();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(con, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                        dial.dismiss();
                                    }
                                });;
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return film.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView jud;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            jud = itemView.findViewById(R.id.tvJud);
        }
    }
}
