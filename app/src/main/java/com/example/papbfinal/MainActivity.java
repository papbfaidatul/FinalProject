package com.example.papbfinal;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.graphics.ColorSpace;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.reflect.MalformedParameterizedTypeException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    EditText jud;
    EditText desc;
    Button bti;
    FirebaseFirestore ff = FirebaseFirestore.getInstance();
    ProgressDialog dial;

    RecyclerView rv;
    List<Film> fl = new ArrayList<>();
    Adapter adapt;
    Button btShow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        jud =findViewById(R.id.etJudul);
        desc  =findViewById(R.id.etDeskripsi);
        bti = findViewById(R.id.btInsert);
        dial = new ProgressDialog(MainActivity.this);
        dial.setTitle("load");

        bti.setOnClickListener(v-> {
            saveData(jud.getText().toString(), desc.getText().toString()
            );
        });

        rv = findViewById(R.id.rv1);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adapt = new Adapter(MainActivity.this, fl);
        btShow = findViewById(R.id.btShow);
        rv.setAdapter(adapt);

        btShow.setOnClickListener(v->{
            show();
        });
    }

    private void saveData(String j, String d) {
        Map<String,Object> film = new HashMap<>();
        String id = UUID.randomUUID().toString();
        film.put("id", id);
        film.put("judul", j);
        film.put("desc", d);

        dial.setMessage("Inserting Data..");
        dial.show();

        ff.collection("Film").document(id)
                .set(film)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getApplicationContext(), "Movie was Succesfully Inserted.", Toast.LENGTH_SHORT).show();
                        dial.dismiss();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        dial.dismiss();
                    }
                });
    }

    private void show(){
        dial.setMessage("Showing Data..");
        dial.show();
        ff.collection("Film")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        Toast.makeText(getApplicationContext(), "Movies Showed.", Toast.LENGTH_SHORT).show();
                        dial.dismiss();
                        if(fl.isEmpty()){
                            dataShowed(task);
                        }
                        else{
                            fl.clear();
                            dataShowed(task);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        dial.dismiss();
                    }
                });
    }

    private void dataShowed(Task<QuerySnapshot> task) {
        for (DocumentSnapshot dc : task.getResult()) {
            Film f = new Film(dc.getString("id"),
                    dc.getString("judul"),
                    dc.getString("desc"));
            fl.add(f);
        }
        adapt.notifyDataSetChanged();
    }
}